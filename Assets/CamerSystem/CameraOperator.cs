﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOperator : MonoBehaviour {

	[Range(0, 1000)]
	public int priority = 4;
	public bool active = true;

	// Use this for initialization
	void Start () {
		this.AssignToDirector ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

//	[ExecuteInEditMode]
	//Update director variables, when any variable in the CameraOperator inspector change
	public void OnValidate() {
		GameObject.FindObjectOfType<Director> ().CameraOperatorReportChanges();
	}

	private void AssignToDirector() {
		GameObject.FindObjectOfType<Director> ().CameraOperatorAdd(this.GetComponent<CameraOperator>());
	}
}
