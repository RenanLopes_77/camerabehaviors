﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Director : MonoBehaviour {

	private ArrayList cameraOperators = new ArrayList();
	public int highestPriority = -1;
	public CameraOperator activeCamera;
	public Camera camera;

	// Use this for initialization
	void Start () {
		this.getActiveCamera ();
	}

	public void CameraOperatorAdd (CameraOperator cameraOperator) {
		this.cameraOperators.Add (cameraOperator);
	}

	public void CameraOperatorRemove (CameraOperator cameraOperator) {
		this.cameraOperators.Remove (cameraOperator);
	}

	private void getActiveCamera () {
		this.highestPriority = -1000;
		foreach(CameraOperator cameraOperator in this.cameraOperators) {
			if (this.activeCamera == null && cameraOperator.active) {
				this.activeCamera = cameraOperator;
				this.highestPriority = cameraOperator.priority;
			} else if (!this.activeCamera.active && cameraOperator.active) {
				this.activeCamera = cameraOperator;
				this.highestPriority = cameraOperator.priority;
			} else if (cameraOperator.active && cameraOperator.priority > this.highestPriority) {
				this.activeCamera = cameraOperator;
				this.highestPriority = cameraOperator.priority;
			}
		}
	}

	// Função pelo operador de camera quando tem suas variáveira alterads
	public void CameraOperatorReportChanges() {
		this.getActiveCamera ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
